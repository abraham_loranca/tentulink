<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Mime\Part\Multipart\RelatedPart;

class Link extends Model
{
    protected $guarded=[];
    public function user()
    {
        return $this->belongsTo(User::class); 
    }

    public function visits()
    {
        return $this->hasMany( Visit::class);
    }
    public function latest_visit()
    {
        return $this->hasOne( Visit::class)->latest();
    }
}
