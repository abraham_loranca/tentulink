#linktree
 
version: v1.0
 
Aplicacion web donde obtendrás links de tus redes sociales favoritas y asi compartirlas todas en uno 
 
 
### SET UP
* Requirements (Already covered with Docker deployment)
	1. Apache/2.4.27 or greater.
	2. MySQL 5.7 or greater.
	3. PHP/7.2.24 or greater.
 
  
* App Configuration
    1. Add host `linktree.localhost`,
        	see [Edit hosts](https://dinahosting.com/ayuda/como-modificar-el-fichero-hosts).        	
    2. Create `.env` file from `example.env` and set it.
	4. Give Folder permissions:	
	    ```
		en linux
	    sudo chown -R $USER:www-data storage;
        chmod -R 775 storage;
        sudo chown -R $USER:www-data bootstrap/cache;
        chmod -R 775 bootstrap/cache;
	    ```
		
	7. Import database from `database/updates/*.sql` into `linktree` DB
        with `root` user, at `localhost` host, `33063` port.
    8. Set `APP_KEY=base64:LU18ueQmODbKhj+oWkJiJ2ozl7w9Bdl24fDMb+rLm+c=` at `.env`.     	
	9. Run `composer install`.
	10. Run `php artisan storage:link`. 
	

* Database Key Fields, tables and or values:
 
	1. `users.email`: Users email.
 
* Git :
    [Gitflow](http://nvie.com/posts/a-successful-git-branching-model).
* Back End:
    [Laravel 7.x](https://laravel.com/docs/7.x),
* Front End:
    [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction),
 
***
 
2021 [Abraham Loranca](abrahamloranca@gmail.com)